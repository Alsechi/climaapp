package com.example.alsechi.climaapp;


import android.Manifest;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Calendar;

import android.support.v4.app.ActivityCompat;

import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;
import android.support.v7.app.AppCompatActivity;





public class MainActivity extends AppCompatActivity {


    Typeface weatherFont;
    TextView cityField;
    TextView updatedField;
    TextView detailsField;
    TextView currentTemperatureField;
    TextView weatherIcon;
    // Holds an instance of the GPSTracker class
    GPSTracker gps;

    // Holds an instance of the Darksky API
    Darksky darksky;

    // Holds weather data for the next 7 days
    ArrayList<Forecast> weeklyForecast;

    public static final String BASE_URL = " https://api.darksky.net/forecast/";
    public static final String API_KEY = "c87fe804129bba146a045f0a9da119fe";

    private static final int PETICION_PERMISO_LOCALIZACION = 101;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //declare at the top if possible
        cityField = (TextView)findViewById(R.id.city_field);
        updatedField = (TextView)findViewById(R.id.updated_field);
        detailsField = (TextView)findViewById(R.id.details_field);
        currentTemperatureField = (TextView) findViewById(R.id.current_temperature_field);
        weatherIcon = (TextView)findViewById(R.id.weather_icon);
       weatherIcon.setTypeface(weatherFont);
        weatherFont = Typeface.createFromAsset(this.getAssets(), "weather.ttf");
       // weatherIcon.setText(getString(R.string.weather_clear_night));


        weeklyForecast = new ArrayList<Forecast>();



        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PETICION_PERMISO_LOCALIZACION);
        }else{ capturargps();}




    }




    private void renderWeather(JSONObject json) {
        try {
            //detailsField.setText("");
            //cityField.setText("");
            Calendar calendar = Calendar.getInstance();
            int today = calendar.get(Calendar.DAY_OF_WEEK);

            String[] days = {"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
            JSONArray data_array = json.getJSONObject("daily").getJSONArray("data");
            for (int i = 0; i < 7; i++) {
                JSONObject item = data_array.getJSONObject(i);

                String temperatureMax = item.getString("temperatureMax");
                String temperatureMin = item.getString("temperatureMin");
                String w_summary = item.getString("summary");
                temperatureMax = temperatureMax.substring(0, 2);
                temperatureMin = temperatureMin.substring(0, 2);

                detailsField.setText(detailsField.getText() + days[(today + i) % 7] + ": " + temperatureMin + " - " + temperatureMax + " " + w_summary + "\n");
            }


            //cityField.setText("New York");
            if (json.getString("timezone").contains("York"))
                cityField.setText("New York");
            if (json.getString("timezone").contains("London"))
                cityField.setText("London");
            if (json.getString("timezone").contains("Los"))
                cityField.setText("Los Angeles");
            if (json.getString("timezone").contains("Paris"))
                cityField.setText("Paris");
            if (json.getString("timezone").contains("Tokyo"))
                cityField.setText("Tokyo");


            currentTemperatureField.setText(json.getJSONObject("currently").getString("temperature") + " \u00b0 F");
            updatedField.setText(

                    // "SUMMARY OF WEEK  : " +
                    json.getJSONObject("daily").getString("summary")
                    // +      "\nTIME ZONE  : " + json.getString("timezone")
            );


        } catch (Exception e) {
            Log.e("SimpleWeather", "One or more fields not found in the JSON data");
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PETICION_PERMISO_LOCALIZACION) {
            if (grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                //Permiso concedido
                capturargps();



            } else {
                //Permiso denegado:
                //Deberíamos deshabilitar toda la funcionalidad relativa a la localización.
                // 1. Instantiate an AlertDialog.Builder with its constructor
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

// 2. Chain together various setter methods to set the dialog characteristics
                builder.setMessage(R.string.dialog_message)
                        .setTitle(R.string.dialog_title);

// 3. Get the AlertDialog from create()
                AlertDialog dialog = builder.create();



            }
        }

    }

    private void capturargps() {



        // Instantiate GPS object
        gps = new GPSTracker(MainActivity.this);

        // Check if GPS is enabled
        if (gps.canGetLocation()) {
            Toast.makeText(getApplicationContext(), "Latitude: " + gps.getLatitude() + " // Longitude: " + gps.getLongitude(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "Unable to get location. :(", Toast.LENGTH_LONG).show();
        }


        if (!isOnline(getApplicationContext())) {

            Toast.makeText(this, "Compruebe su conexión a internet",Toast.LENGTH_SHORT).show();
        }else{
        RequestQueue queue = Volley.newRequestQueue(this);
        String url =BASE_URL + API_KEY + "/" +gps.getLatitude() + "," + gps.getLongitude();

// Request a string response from the provided URL.
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject root = response;
                            JSONObject daily = root.getJSONObject("daily");
                            renderWeather(root);

                            JSONArray forecasts = daily.getJSONArray("data");
                            weeklyForecast.clear();

                            // Loop through every day
                            for (int i = 0; i < forecasts.length(); i++) {
                                JSONObject dayObject = forecasts.getJSONObject(i);
                                Log.d("dia",dayObject.toString());
                                weeklyForecast.add(new Forecast(
                                        dayObject.getInt("time"),
                                        dayObject.getString("summary"),
                                        dayObject.getString("icon"),
                                        dayObject.getDouble("temperatureMin"),
                                        dayObject.getDouble("temperatureMax"),
                                        dayObject.getDouble("windSpeed"),
                                        dayObject.getDouble("humidity")
                                ));
                            }
                            Log.d("resultado",weeklyForecast.get(2).toString());




                        } catch(Exception e) {
                            // TODO: handle
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);

        // Set up API requester with location data
        //  darksky = new Darksky(gps.getLatitude(), gps.getLongitude(),this);



        Log.d("resultado2",weeklyForecast.toString());
    }
    }


    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }
}
