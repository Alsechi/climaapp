package com.example.alsechi.climaapp;

/**
 * Created by jorgealejandro on 12/07/2017.
 */

public class Forecast {
    public int time;
    public String summary;
    public String icon;
    public double temperatureMin;
    public double tempreatureMax;
    public double windSpeed;
    public double humidity;

    public Forecast(int time, String summary, String icon, double temperatureMin, double temperatureMax, double windSpeed, double humidity) {
        this.time = time;
        this.summary = summary;
        this.icon = icon;
        this.temperatureMin = temperatureMin;
        this.tempreatureMax = temperatureMax;
        this.windSpeed = windSpeed;
        this.humidity = humidity;
    }

    @Override
    public String toString() {
        return "Forecast{" +
                "time=" + time +
                ", summary='" + summary + '\'' +
                ", icon='" + icon + '\'' +
                ", temperatureMin=" + temperatureMin +
                ", tempreatureMax=" + tempreatureMax +
                ", windSpeed=" + windSpeed +
                ", humidity=" + humidity +
                '}';
    }
}
