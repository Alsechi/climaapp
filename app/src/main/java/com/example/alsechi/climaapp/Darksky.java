package com.example.alsechi.climaapp;



import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

/**
 * Created by jorgealejandro on 12/07/2017.
 */

public class Darksky  {
    // https://api.darksky.net/forecast/[key]/[latitude],[longitude]

    public static final String BASE_URL = " https://api.darksky.net/forecast/";
    public static final String API_KEY = "c87fe804129bba146a045f0a9da119fe";
    public Context context;

    // Location data
    protected double latitude;
    protected double longitude;

    // Holds weather information for the entire week
    ArrayList<Forecast> weeklyForecast = new ArrayList<Forecast>();



    public Darksky(double latitude, double longitude, MainActivity mainActivity) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.context=mainActivity;



        RequestQueue queue = Volley.newRequestQueue(context);
        String url =BASE_URL + API_KEY + "/" + latitude + "," + longitude;

// Request a string response from the provided URL.
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject root = response;
                            JSONObject daily = root.getJSONObject("daily");
                            JSONArray forecasts = daily.getJSONArray("data");
                            weeklyForecast.clear();

                            // Loop through every day
                            for (int i = 0; i < forecasts.length(); i++) {
                                JSONObject dayObject = forecasts.getJSONObject(i);
                                Log.d("dia",dayObject.toString());
                                weeklyForecast.add(new Forecast(
                                        dayObject.getInt("time"),
                                        dayObject.getString("summary"),
                                        dayObject.getString("icon"),
                                        dayObject.getDouble("temperatureMin"),
                                        dayObject.getDouble("temperatureMax"),
                                        dayObject.getDouble("windSpeed"),
                                        dayObject.getDouble("humidity")
                                ));
                            }
                            Log.d("resultado",weeklyForecast.get(2).toString());


                        } catch(Exception e) {
                            // TODO: handle
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);





    }

    public ArrayList<Forecast> getWeeklyForecast() {

        return weeklyForecast;

    }




}
